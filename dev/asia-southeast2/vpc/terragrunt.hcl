terraform {
  source = "git::https://github.com/gruntwork-io/terraform-google-network.git//modules/vpc-network?ref=v0.4.0"
}

include {
  path = find_in_parent_folders()
}

locals {
  name = "terragrunt-cluster"
}

inputs = {
  name_prefix          = local.name
  cidr_block           = "10.20.0.0/16"
  secondary_cidr_block = "10.30.0.0/16"
}

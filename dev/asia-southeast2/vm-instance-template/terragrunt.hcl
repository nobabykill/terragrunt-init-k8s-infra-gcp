terraform {
  source = "git::https://github.com/terraform-google-modules/terraform-google-vm.git//modules/instance_template?ref=v5.1.0"
}

include {
  path = find_in_parent_folders()
}

dependency "vpc" {
  config_path = "../vpc"
}

inputs = {
  machine_type = "g1-small"
  preemptible  = true
  service_account = {
    email  = try(google_service_account.service_account[0].email, null)
    scopes = ["cloud-platform"]
  }
  name_prefix = "demo-template"
  network     = dependency.vpc.outputs.network
  subnetwork  = dependency.vpc.outputs.private_subnetwork
}

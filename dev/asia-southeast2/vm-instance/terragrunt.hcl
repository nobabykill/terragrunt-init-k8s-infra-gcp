terraform {
  source = "git::https://github.com/terraform-google-modules/terraform-google-vm.git//modules/compute_instance?ref=v5.1.0"
}

include {
  path = find_in_parent_folders()
}

dependency "vm_instance_template" {
  config_path = "../vm-instance-template"
}

dependency "vpc" {
  config_path = "../vpc"
}

inputs = {
  instance_template = dependency.vm_instance_template.outputs.self_link
  network           = dependency.vpc.outputs.network
  subnetwork        = dependency.vpc.outputs.private_subnetwork
}
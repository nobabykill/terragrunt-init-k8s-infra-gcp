terraform {
  source = "git::https://github.com/terraform-google-modules/terraform-google-kubernetes-engine.git?ref=v11.1.0"
}

include {
  path = find_in_parent_folders()
}

dependency "vpc" {
  config_path = "../vpc"
}

locals {
  cluster_name = "lotusfarm-cluster"
  region = "asia-southeast2"
}

inputs = {
  name                       = local.cluster_name
  network                    = "lotusfarm-vpc-network"
  subnetwork                 = dependency.vpc.outputs.private_subnetwork_name
  ip_range_pods              = dependency.vpc.outputs.private_subnetwork_secondary_range_name
  ip_range_services          = dependency.vpc.outputs.private_subnetwork_secondary_range_name
  http_load_balancing        = true
  horizontal_pod_autoscaling = true
  kubernetes_version = "latest"

  node_pools = [
    {
      name               = "node-pool-1"
      machine_type       = "e2-standard-2"
      node_locations     = local.region
      min_count          = 2
      max_count          = 2
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = true
      initial_node_count = 2
      #service_account = try(google_service_account.service_account[0].email, null)
    }
    #{
    #  name               = "node-pool-2"
    #  machine_type       = "e2-medium"
    #  node_locations     = "asia-southeast2-b"
    #  min_count          = 1
    #  max_count          = 1
    #  local_ssd_count    = 0
    #  disk_size_gb       = 100
    #  disk_type          = "pd-standard"
    #  image_type         = "COS"
    #  auto_repair        = true
    #  auto_upgrade       = true
    #  preemptible        = true
    #  initial_node_count = 1
    #  #service_account = try(google_service_account.service_account[0].email, null)
    #}
  ]

  node_pools_oauth_scopes = {
    all = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }

  node_pools_labels = {
    all = {}

    node-pool-1 = {
      pool-1 = true
    }

    node-pool-2 = {
      pool-2 = true
    }
  }

  node_pools_metadata = {
    all = {}
  }

  node_pools_taints = {
    all = []
  }

  node_pools_tags = {
    all = ["allow-monitor", "allow-vpn"]
  }
}

# Set account-wide variables. These are automatically pulled in to configure the remote state bucket in the root
# terragrunt.hcl configuration.
locals {
  project_name = "lotusfarm-project"
  project      = "lotusfarm-project"
  project_id   = "lotusfarm-project"
  bucket       = "terragrunt-lotusfarm-project-infra-29122020"
}

# ---------------------------------------------------------------------------------------------------------------------
# TERRAGRUNT CONFIGURATION
# Terragrunt is a thin wrapper for Terraform that provides extra tools for working with multiple Terraform modules,
# remote state, and locking: https://github.com/gruntwork-io/terragrunt
# ---------------------------------------------------------------------------------------------------------------------

locals {
  # Automatically load account-level variables
  account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"))

  # Automatically load region-level variables
  region_vars = read_terragrunt_config(find_in_parent_folders("region.hcl"))

  # Automatically load environment-level variables
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))

  # Extract the variables we need for easy access
  project_name = local.account_vars.locals.project_name
  project_id   = local.account_vars.locals.project_id
  bucket       = local.account_vars.locals.bucket
  region       = local.region_vars.locals.region
  env          = local.environment_vars.locals.environment
}

# Generate an google provider block
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"

  contents = <<EOF
provider "google" {
  version     = "~> 3.43.0"
  project     = "${local.project_id}"
  region      = "${local.region}"
}
EOF
}


# Configure Terragrunt to automatically store tfstate files in an S3 bucket
remote_state {
  backend = "gcs"

  config = {
    prefix   = "${path_relative_to_include()}/terraform.tfstate"
    bucket   = local.bucket
    project  = local.project_id
    location = local.region
    # credentials = "/home/linh/Downloads/spinnaker-study-7ecd57a3f5e2.json"
  }
}

terraform_version_constraint = ">= 0.13.3"


# ---------------------------------------------------------------------------------------------------------------------
# GLOBAL PARAMETERS
# These variables apply to all configurations in this subfolder. These are automatically merged into the child
# `terragrunt.hcl` config via the include block.
# ---------------------------------------------------------------------------------------------------------------------

# Configure root level variables that all resources can inherit. This is especially helpful with multi-account configs
# where terraform_remote_state data sources are placed directly into the modules.

inputs = merge(
  local.account_vars.locals,
  local.region_vars.locals,
  local.environment_vars.locals,
)
